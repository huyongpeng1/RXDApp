package com.youngport.app.rxdapp.abstractfactory;

/**
 * Created by popper on 2017/12/26.
 */

public class WXFactory implements Provider<WXSender> {
    @Override
    public WXSender produce() {
        return new WXSender();
    }
}
