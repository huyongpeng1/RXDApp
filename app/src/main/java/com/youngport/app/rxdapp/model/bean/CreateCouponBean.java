package com.youngport.app.rxdapp.model.bean;

/**
 * Created by popper on 2017/12/4.
 */
public class CreateCouponBean {
    public String code;
    public String msg;

    /**
     * data : 恭喜你添加成功
     */

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
