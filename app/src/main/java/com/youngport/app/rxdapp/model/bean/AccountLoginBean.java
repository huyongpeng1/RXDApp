package com.youngport.app.rxdapp.model.bean;

import android.databinding.BaseObservable;
import android.databinding.Bindable;



/**
 * Created by admin on 2017/6/21.
 */

public class AccountLoginBean /*extends BaseObservable */{

    /**
     * code : success
     * msg : 登录成功！
     * userInfo : {"uid":"488","role_id":"3","user_phone":"18319437579","voice_open":"1","is_open":"1","status":"1","reset_pwd":"0","role_full_name":"后会无期虚脱",
     * "auth_report":"0","auth_bill":"0","auth_goods":"0","auth_member":"0","auth_coupon":"0","add_time":1498026156,"token":"1C3C464CC4E7F99F6FD74B2EA51A4752B73C6C4C"}
     */


    /**
     * userInfo : {"add_time":1501040399,"auth":"","is_open":"1","mac_id":"9","reset_pwd":"1","role_full_name":"洋仆淘大商户","role_id":"3","status":"1","token":"094E11AC2387191359573417DE7D296A54715792","uid":"26","user_phone":"17771507422","voice_open":"1"}
     */

    /**
     * add_time : 1501040399
     * auth :
     * is_open : 1
     * mac_id : 9
     * reset_pwd : 1
     * role_full_name : 洋仆淘大商户
     * role_id : 3
     * status : 1
     * token : 094E11AC2387191359573417DE7D296A54715792
     * uid : 26
     * user_phone : 17771507422
     * voice_open : 1
     */

    private int add_time;
    private String auth;
    private String is_open;
    private String mac_id;
    private String reset_pwd;
    public String role_full_name;
    private String role_id;
    private String status;
    private String token;
    private String uid;
    private String user_phone;
    private String voice_open;

    public int getAdd_time() {
        return add_time;
    }

    public void setAdd_time(int add_time) {
        this.add_time = add_time;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getIs_open() {
        return is_open;
    }

    public void setIs_open(String is_open) {
        this.is_open = is_open;
    }

    public String getMac_id() {
        return mac_id;
    }

    public void setMac_id(String mac_id) {
        this.mac_id = mac_id;
    }

    public String getReset_pwd() {
        return reset_pwd;
    }

    public void setReset_pwd(String reset_pwd) {
        this.reset_pwd = reset_pwd;
    }

//    @Bindable
    public String getRole_full_name() {
        return role_full_name;
    }

    public void setRole_full_name(String role_full_name) {
        this.role_full_name = role_full_name;
//        notifyPropertyChanged(BR.role_full_name);
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getVoice_open() {
        return voice_open;
    }

    public void setVoice_open(String voice_open) {
        this.voice_open = voice_open;
    }

}
