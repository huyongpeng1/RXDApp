package com.youngport.app.rxdapp.presenter.contract;

import com.youngport.app.rxdapp.base.BaseView;

/**
 * Created by popper on 2017/12/26.
 */

public interface ScaleSelectorContract {
    public interface View extends BaseView {
    }

    public interface Presenter {
    }
}
