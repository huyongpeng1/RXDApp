package com.youngport.app.rxdapp.abstractfactory;

/**
 * Created by popper on 2017/12/26.
 */

public interface Provider<T> {
    public T produce();
}
