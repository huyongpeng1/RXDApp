package com.youngport.app.rxdapp.interfaces;

import android.view.View;

/**
 * Created by popper on ${DATA}.
 */
public interface ViewOnclickInterface {
    void onClickInterface(View view);
}
