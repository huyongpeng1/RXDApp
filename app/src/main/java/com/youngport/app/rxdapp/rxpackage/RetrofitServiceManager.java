package com.youngport.app.rxdapp.rxpackage;


import com.youngport.app.rxdapp.constant.Constant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 2017/4/6.
 * Retrofitmanager用于创建retrofit
 * 使用RetrofitManager.getContext().create();
 */

public class RetrofitServiceManager {
    private Retrofit mRetrofit;
    private static RetrofitServiceManager mManager;

    public RetrofitServiceManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(Constant.CONNECT_TIME_OUT, TimeUnit.SECONDS);
        builder.readTimeout(Constant.READ_TIME_OUT, TimeUnit.SECONDS);
        // 添加公共参数拦截器
        if (Constant.IF_INTERCEPT_HTTP_LOG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addNetworkInterceptor(interceptor);
        }
        mRetrofit = new Retrofit.Builder()
                .client(builder.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constant.YOUNGPORT_BASE_URL)
                .build();
    }


    /**
     * 获取RetrofitServiceManager
     *
     * @return
     */
    public static RetrofitServiceManager getInstance() {
        if (mManager == null) {
            synchronized (RetrofitServiceManager.class) {
                if (mManager == null) {
                    mManager = new RetrofitServiceManager();
                }
            }
        }
        return mManager;
    }

    /**
     * 获取对应的Service
     *
     * @param service Service 的 class
     */
    public <T> T create(Class<T> service) {
        return mRetrofit.create(service);
    }


}
