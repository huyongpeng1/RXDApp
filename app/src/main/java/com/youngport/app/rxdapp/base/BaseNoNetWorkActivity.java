package com.youngport.app.rxdapp.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.youngport.app.rxdapp.utils.AtyContainer;
import com.youngport.app.rxdapp.utils.ToastUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by popper on ${DATA}.
 */
public abstract class BaseNoNetWorkActivity extends AppCompatActivity {
    private Unbinder mBind;
    private int mLayoutId;
    public View mInflate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInflate = LayoutInflater.from(this).inflate(getLayoutId(), null);
        setContentView(mInflate);
        mBind = ButterKnife.bind(this);
        AtyContainer.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    protected abstract void initListener();

    protected abstract void initData();

    protected abstract void initView();

    public abstract int getLayoutId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastUtil.destoryToast();
        mBind.unbind();
        AtyContainer.getInstance().removeActivity(this);
    }

    public String getEditTextContent(EditText editText) {
        return editText.getText().toString().trim();
    }

    public String getTextContent(TextView textView) {
        return textView.getText().toString().trim();
    }

}
