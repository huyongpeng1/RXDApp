package com.youngport.app.rxdapp.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.youngport.app.rxdapp.dagger.component.ActivityComponent;

import com.youngport.app.rxdapp.dagger.component.DaggerActivityComponent;
import com.youngport.app.rxdapp.dagger.module.ActivityModule;
import com.youngport.app.rxdapp.dagger.module.AppModule;
import com.youngport.app.rxdapp.utils.AtyContainer;
import com.youngport.app.rxdapp.utils.ToastUtil;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by popper on 2017/11/20.
 */

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements BaseView {
    @Inject//被@Inject标注的对象不能用private修饰
    public T mPresenter;
    private Unbinder mBind;
    public View mView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = LayoutInflater.from(this).inflate(getLayoutId(), null);
        setContentView(mView);
        mBind = ButterKnife.bind(this);
        initInject();
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        AtyContainer.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastUtil.destoryToast();
        mBind.unbind();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        AtyContainer.getInstance().removeActivity(this);
    }

    public ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder().appModule(new AppModule()).activityModule(new ActivityModule(this)).build();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public String getEditTextContent(EditText editText) {
        return editText.getText().toString().trim();
    }

    public String getTextContent(TextView textView) {
        return textView.getText().toString().trim();
    }

    protected abstract void initInject();

    protected abstract void initListener();

    protected abstract void initData();

    protected abstract void initView();

    public abstract int getLayoutId();
}
