package com.youngport.app.rxdapp.base;

/**
 * Created by popper on ${DATA}.
 */
public class BaseBean<T> {
    public String code;
    public String msg;
    public T userInfo;
}
