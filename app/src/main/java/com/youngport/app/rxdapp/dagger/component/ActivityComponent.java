package com.youngport.app.rxdapp.dagger.component;

import com.youngport.app.rxdapp.base.BaseActivity;
import com.youngport.app.rxdapp.dagger.scope.ActivityScope;
import com.youngport.app.rxdapp.view.activity.AbstractFactoryActivity;
import com.youngport.app.rxdapp.view.activity.ClassificationActivity;
import com.youngport.app.rxdapp.view.activity.MainActivity;
import com.youngport.app.rxdapp.dagger.module.ActivityModule;
import com.youngport.app.rxdapp.dagger.module.AppModule;
import com.youngport.app.rxdapp.view.activity.NewCouponCardActivity;
import com.youngport.app.rxdapp.view.activity.ScaleSelectorActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by popper on 2017/11/20.
 */
//Component 是Module 和Inject的桥梁，是将Module提供的参数，注入到@Inject标注的对象中，inject方法的参数必须是具体实现类，
// 它是在参数中找需要注入的对象，不能是父类
//因为@Inject标注在具体的实现类中；Component也叫Injector
@Singleton
@ActivityScope
@Component(/*dependencies = AppModule.class,*/ modules = {ActivityModule.class, AppModule.class})
public interface ActivityComponent {
    void inject(MainActivity mainActivity);

    void inject(NewCouponCardActivity newCouponCardActivity);

    void inject(AbstractFactoryActivity abstractFactoryActivity);

    void inject(ClassificationActivity classificationActivity);

    void inject(ScaleSelectorActivity scaleSelectorActivity);
}
