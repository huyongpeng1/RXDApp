package com.youngport.app.rxdapp.utils;


import com.youngport.app.rxdapp.constant.Constant;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by admin on 2017/4/15.
 */

public class MD5Util {
    /**
     * MD5 32
     */
    public static String md5(String... array) {
        //sort item
        StringBuilder sb = new StringBuilder();
        Collections.sort(Arrays.asList(array), new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });
        for (int i = 0; i < array.length; i++) {
            if (i < array.length - 1) {
                sb.append(array[i]).append("&");
            } else {
                sb.append(array[i]).append("&key=").append(Constant.KEY);
            }
        }
        //md5
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(sb.toString().getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10)
                hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString().toUpperCase();
    }

}
