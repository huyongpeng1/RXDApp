package com.youngport.app.rxdapp.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;

/**
 * Created by willkernel on 2016/6/21.
 * Email:willkerneljc@gmail.com
 */
@SuppressWarnings("unused")
public class SpannableUtil {
    private SpannableString spannableString;
    private Resources resources;

    public SpannableUtil(Context context) {
        resources = context.getResources();
    }

    public SpannableUtil setText(int strId) {
        spannableString = new SpannableString(resources.getString(strId));
        return this;
    }

    public SpannableUtil setText(String str) {
        spannableString = new SpannableString(str);
        return this;
    }

    /**
     * @param size sp/dp
     */
    public SpannableUtil setTextSize(int size, int start, int end) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        spannableString.setSpan(new AbsoluteSizeSpan(size, true), start, end, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }

    /**
     * @param size sp/dp
     */
    public SpannableUtil setTextSize(int size, int start) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        spannableString.setSpan(new AbsoluteSizeSpan(size, true), start, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }

    @SuppressWarnings("deprecation")
    public SpannableUtil setTextColor(int color, int start, int end) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        spannableString.setSpan(new ForegroundColorSpan(resources.getColor(color)), start, end, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }

    @SuppressWarnings("deprecation")
    public SpannableUtil setTextColor(int color, int start) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        spannableString.setSpan(new ForegroundColorSpan(resources.getColor(color)), start, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }
    public SpannableUtil setTextColor(String color, int start) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor(color)), start, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }

    @SuppressWarnings("deprecation")
    public SpannableUtil setTextDrawable(int drawableId, int start) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        Drawable drawable = resources.getDrawable(drawableId);
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
        spannableString.setSpan(new ImageSpan(drawable), start, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }

    @SuppressWarnings("deprecation")
    public SpannableUtil setTextDrawable(int drawableId, int start, int end) {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        Drawable drawable = resources.getDrawable(drawableId);
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
        spannableString.setSpan(new ImageSpan(drawable), start, end, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return this;
    }

    public SpannableString build() {
        if (spannableString == null) {
            throw new NullPointerException("not yet set spannableString!");
        }
        return spannableString;
    }

}