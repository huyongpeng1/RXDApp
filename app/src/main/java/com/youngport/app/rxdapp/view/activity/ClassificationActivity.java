package com.youngport.app.rxdapp.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.youngport.app.rxdapp.R;
import com.youngport.app.rxdapp.base.BaseActivity;
import com.youngport.app.rxdapp.presenter.ClassificationPresenter;
import com.youngport.app.rxdapp.presenter.contract.ClassificationContract;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by popper on 2017/12/26.
 */

public class ClassificationActivity extends BaseActivity<ClassificationPresenter> implements ClassificationContract.View {
    @BindView(R.id.btn_appinfo)
    Button mBtnAppinfo;
    @BindView(R.id.btn_upload_img)
    Button mBtnUploadImg;
    @BindView(R.id.btn_abstract_factory)
    Button mBtnAbstractFactory;

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_classification;
    }


    @OnClick({R.id.btn_appinfo, R.id.btn_upload_img, R.id.btn_abstract_factory})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_appinfo:
                startActivity(new Intent(this, AppInfoActivity.class));
                break;
            case R.id.btn_upload_img:
                startActivity(new Intent(this, NewCouponCardActivity.class));
                break;
            case R.id.btn_abstract_factory:
                startActivity(new Intent(this, AbstractFactoryActivity.class));
                break;
        }
    }
}
