package com.youngport.app.rxdapp.view.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.youngport.app.rxdapp.R;
import com.youngport.app.rxdapp.base.BaseActivity;
import com.youngport.app.rxdapp.constant.Constant;
import com.youngport.app.rxdapp.databinding.ActivityMainBinding;
import com.youngport.app.rxdapp.interfaces.ViewOnclickInterface;
import com.youngport.app.rxdapp.model.bean.AccountLoginBean;
import com.youngport.app.rxdapp.presenter.MainActivityPresenter;
import com.youngport.app.rxdapp.presenter.contract.MainActivityContract;
import com.youngport.app.rxdapp.utils.CreateTimestamp;
import com.youngport.app.rxdapp.utils.PreferenceUtil;
import com.youngport.app.rxdapp.utils.SpannableUtil;
import com.youngport.app.rxdapp.utils.ToastUtil;

import butterknife.BindView;
import butterknife.OnClick;
import cn.cloudcore.iprotect.plugin.CEditTextAttrSet;


public class MainActivity extends BaseActivity<MainActivityPresenter> implements MainActivityContract.View, ViewOnclickInterface {
    @BindView(R.id.tv_cancle)
    TextView mTvCancle;
    @BindView(R.id.imageview)
    ImageView mImageview;
    @BindView(R.id.edt_phone_number)
    EditText mEdtPhoneNumber;
    @BindView(R.id.iv_cancle)
    ImageView mIvCancle;
    @BindView(R.id.imageview_second)
    ImageView mImageviewSecond;
    /*@BindView(R.id.edt_password)
    CEditText mEdtPassword;*/
    @BindView(R.id.iv_show_password)
    ImageView mIvShowPassword;
    @BindView(R.id.tv_saoma_login)
    TextView mTvSaomaLogin;
    @BindView(R.id.tv_forget_password)
    TextView mTvForgetPassword;
    private ActivityMainBinding mBind;
    String receiver_action = "cn.cloudcore.iprotect.plugin.ckbd";
    private TextReceiver commandReceiver;
    private String modulus = "da2ebfba8e3fe52dd8b582485ce4ca8e96b8f7350067d4741b455813b1d7adc0e3b3bf1eb137194552a0711e3e068d91cc377d307633a7815b9fee76187d2e404761e0b6286f8cb228765b4c9fc89741929071dcfb849cf8ea526ead941e6dcc22516b2f46cb61f6c952996bb906410c279c171d12ed9a20353f932e7d6536a1";
    //设置公钥模数的地方
    private String modulus_app = "9f12f4c673804908d4db1cf1501f118eb779e7b4746c8f7c42caaeea78920bd56595fb90898e534a7ffbfd38ba7926d147d18e77912c9148ec6f81ea09e344fa9d9eff5bc0f545dd9e9820bd0fbe65a60d8ed02e52d5027f143bbfc2d79220cc068d0a3e4d9a75b9f0c2134efca99a11b961de328b986ff83da2ae9422faf021";
    private String modulus_enc = "a0661982beb093b0ccc2af5987a19ba6f344a283c68b3bc27965e2fc1def3cf8a730675c667c99b3691731ca882c75409bc391593128a01fdcd3d0383678060aeefe3afe5347ad334a4d8016ee5e967deb07c2054aa681d725bd4a50b97fa8294eb41e29c88cefc86d911d57572f410570525f8c1c4b910d3fb1c40c88a5d3e3";
    private String eccX = "36589faa2cfb61d807678be9d5e15d74078b012e153ca99ba98fc22ad82fd138";
    private String eccY = "e8fc8722154aded66f00e62aaea303f30833ee36f1f28339c484adef84e03769";


    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
        mBind = DataBindingUtil.bind(mView);
    }

    @Override
    protected void initListener() {
        mBind.setOnClick2(this);
        mBind.setOnClick1(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long timestamp = System.currentTimeMillis() /*/ 1000*/;
                byte[] ccode = new byte[6];
                ccode[0] = (byte) 5;
                ccode[1] = (byte) 10;
                ccode[2] = (byte) 11;
                ccode[3] = (byte) 11;
                ccode[4] = (byte) 11;
                ccode[5] = (byte) 11;

                mBind.edtPassword.setChallengeCode(ccode);
                byte[] s = mBind.edtPassword.getEncryptedPinCode();
                //Log.e("getEncryptedPinCode", s);

                mBind.edtPassword.setMode(CEditTextAttrSet.WORK_MODE_FOR_DEVELOPMENT);
                mBind.edtPassword.setCalcFactor("21234567890123456");
                mBind.edtPassword.setHashRandom("1:" + "1234567");
                //设置公钥模数的地方
                mBind.edtPassword.publicKeyAppModulus("9f12f4c673804908d4db1cf1501f118eb779e7b4746c8f7c42caaeea78920bd56595fb90898e534a7ffbfd38ba7926d147d18e77912c9148ec6f81ea09e344fa9d9eff5bc0f545dd9e9820bd0fbe65a60d8ed02e52d5027f143bbfc2d79220cc068d0a3e4d9a75b9f0c2134efca99a11b961de328b986ff83da2ae9422faf021");
                mBind.edtPassword.setAlgorithmCode("004");
                Log.d("way", "onClick: " + timestamp);
                String value = mBind.edtPassword.getValue(timestamp + "");
                Log.d("way", "onClick: " + value);
            }
        });
/*mBind.btnLogin.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

    }
});*/
    }

    @Override
    protected void initData() {
        commandReceiver = new TextReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(receiver_action);
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(commandReceiver, filter);
        CEditTextAttrSet attrSets = new CEditTextAttrSet();
        attrSets.name = "password";
        attrSets.textColor = Color.BLACK;
//        attrSets.softkbdView = 0;
//        attrSets.softkbdStype = CEditTextAttrSet.SOFT_KBD_QWERTY_ALPHA_LOWER;
        attrSets.softkbdStype = CEditTextAttrSet.SOFT_KBD_QWERTY_ALPHA_LOWER;
        attrSets.softkbdMode = CEditTextAttrSet.SOFT_KBD_MODE_KDOWN_CHANGE;
        attrSets.switchMode = CEditTextAttrSet.SOFT_KBD_SWITCH_BETWEEN_ALPHA_SYMBOL;
//        attrSets.softkbdMode = CEditTextAttrSet.SOFT_KBD_MODE_KDOWN_NOPOP;
        attrSets.kbdRandom = true;
        attrSets.clearWhenOpenKbd = false;
        attrSets.kbdVibrator = false;
        attrSets.contentType = 0;
        attrSets.maxLength = 30;
        attrSets.maskChar = '*';
        attrSets.accepts = "[!-~]+";
        attrSets.isZoomOut = true;
        mBind.edtPassword.initialize(attrSets);
        mBind.edtPassword.publicKeyAppModulus(modulus_app);   // 一次加密  -- 云核标准数字信封
        //password.publicKeyECC(eccX, eccY);       // 二次加密  -- 国密SM2算法
        mBind.edtPassword.publicKeyModulus(modulus_enc);    // 二次加密  -- RSA算法，根据加密机算法确定
        //password.publicKeyDER(derEncrypt);
        mBind.edtPassword.setAlgorithmCode("001");

        // 密文格式为：长度(4字节)||C1X(32字节)||C1Y(32字节)||C2(4字节对齐)||C3(32字节)
        mBind.edtPassword.publicKeyECC(eccX, eccY);       // 一次加密  国密SM2算法
    }

    @BindingAdapter({"change"})
    public static void changeText(TextView textView, String change) {
        SpannableString build = new SpannableUtil(textView.getContext()).setText(change)
                .setTextColor(R.color.colorAccent, 1).build();
        textView.setText(build);
    }

    @Override
    protected void initView() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    public void onClickInterface(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                mPresenter.performAccountLogin(CreateTimestamp.getTimestamp(), getEditTextContent(mEdtPhoneNumber), getEditTextContent(mBind.edtPassword));
                ToastUtil.showToastCenter("按钮");
//                startActivity(new Intent(this, AppInfoActivity.class));
                break;
            case R.id.tv_saoma_login:
                ToastUtil.showToastCenter("扫码登录");
                break;
        }
    }


   /* *//* @OnClick(R.id.btn_login)
         public void onClick() {
             mPresenter.performAccountLogin(CreateTimestamp.getTimestamp(), getEditTextContent(mEdtPhoneNumber), getEditTextContent(mEdtPassword));
             //        startActivity(new Intent(this, AppInfoActivity.class));
         }*/
    public class TextReceiver extends BroadcastReceiver {
        String updateInfo = "UpdateInfo";
        String closeInfo = "CloseInfo";
        String cKbdInfo;

        int kbdHandle;

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(receiver_action)) {

                Log.e("onReceive", "CKbdName:" + intent.getStringExtra("CKbdName"));
                Log.e("onReceive", "CKbdInfo:" + intent.getStringExtra("CKbdInfo"));
                Log.e("onReceive", "CKbdFlag:" + intent.getStringExtra("CKbdFlag"));
                Log.e("onReceive", "over");

                String cKbdName = intent.getStringExtra("CKbdName");
                String cKbdInfo = intent.getStringExtra("CKbdInfo");

                Log.e("onReceive", "degree:" + mBind.edtPassword.getComplexDegree());


                if (cKbdName.equals("password")) {
                    ToastUtil.showToastCenter("2 info:" + cKbdInfo);

                    if (cKbdInfo.equals("CloseInfo")) {
                        mBind.edtPassword.clearFocus();
                    }
                }

            }
        }
    }

    @Override
    public void accountLoginSuccess(AccountLoginBean accountLoginBean) {
        mBind.setAccountLoginBean(accountLoginBean);
        ToastUtil.showToastCenter("登陆成功！");
        PreferenceUtil.putString(this, Constant.TOKEN_KEY, accountLoginBean.getToken());
//        startActivity(new Intent(this, ClassificationActivity.class));
    }
}
