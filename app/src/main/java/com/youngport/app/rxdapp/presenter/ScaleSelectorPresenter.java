package com.youngport.app.rxdapp.presenter;

import android.content.Context;

import com.youngport.app.rxdapp.mode.ScaleSelectorMode;
import com.youngport.app.rxdapp.presenter.contract.ScaleSelectorContract;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.RXPresenter;
import com.youngport.app.rxdapp.rxpackage.api.PosService;

import javax.inject.Inject;

/**
 * Created by popper on 2017/12/26.
 */

public class ScaleSelectorPresenter extends RXPresenter<ScaleSelectorContract.View, ScaleSelectorMode> implements ScaleSelectorContract.Presenter {
    @Inject
    public ScaleSelectorPresenter(ApiClass posService, Context context) {
        super(posService, context);
    }

    @Override
    public ScaleSelectorMode getModeInstance() {
        return new ScaleSelectorMode();
    }
}
