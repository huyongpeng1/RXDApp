package com.youngport.app.rxdapp.presenter;

import android.content.Context;

import com.youngport.app.rxdapp.constant.Constant;
import com.youngport.app.rxdapp.mode.NewCouponCardMode;
import com.youngport.app.rxdapp.model.bean.CreateCouponBean;
import com.youngport.app.rxdapp.presenter.contract.NewCouponCardContract;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.RXPresenter;
import com.youngport.app.rxdapp.rxpackage.RequestListener;
import com.youngport.app.rxdapp.rxpackage.api.PosService;
import com.youngport.app.rxdapp.utils.ToastUtil;

import java.util.Map;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by popper on 2017/12/4.
 */

public class NewCouponCardPresenter extends RXPresenter<NewCouponCardContract.View, NewCouponCardMode> implements NewCouponCardContract.Presenter {
    @Inject
    public NewCouponCardPresenter(ApiClass posService, Context context) {
        super(posService, context);
    }

    @Override
    public void createNewCouponCard(Map<String, RequestBody> partMap, MultipartBody.Part... part) {
/*        Observable<CreateCouponBean> couponObservable = mPosService.createCoupon(partMap, part);
        doHttpDeal(mContext, couponObservable, new RequestListener<CreateCouponBean>() {

            @Override
            public void onSuccess(CreateCouponBean createCouponBean) {
                if (Constant.SUCCESS.equals(createCouponBean.code)) {
                    ToastUtil.showToastCenter("创建成功！");
                } else {
                    ToastUtil.showToastCenter("失败！");
                }
            }

            @Override
            public void onError(Throwable e) {

            }
        }, false);*/
    }

    @Override
    public NewCouponCardMode getModeInstance() {
        return new NewCouponCardMode();
    }
}
