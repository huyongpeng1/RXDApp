package com.youngport.app.rxdapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by admin on 2017/3/21.
 */
//工具类,用来保存配置和回显

    public class PreferenceUtil {
        public static SharedPreferences getSp(Context context) {
            //指定保存配置文件的名字和模式
            return context.getSharedPreferences("config", 0);
        }
        public static void putBoolean(Context context, String key, boolean value) {
            getSp(context).edit().putBoolean(key, value).commit();
        }
        public static boolean getBoolean(Context context, String key) {
            return getBoolean(context, key, false);
        }
        public static boolean getBoolean(Context context, String key,
                                         boolean defValue) {
            return getSp(context).getBoolean(key, defValue);
        }
        public static void putString(Context context,String key,String value){
            getSp(context).edit().putString(key, value).commit();
        }
        public static String getString(Context context,String key ,String defValue){
            return getSp(context).getString(key, defValue);
        }
        public static String getString(Context context,String key ){
            return getString(context , key, "");
        }
    }

