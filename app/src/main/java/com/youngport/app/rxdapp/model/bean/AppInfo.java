package com.youngport.app.rxdapp.model.bean;

import android.graphics.drawable.Drawable;

/**
 * Created by popper on 2017/11/23.
 */
public class AppInfo {
    public String name;
    public String packageName;
    public Drawable icon;

    public AppInfo(String name, String packageName, Drawable icon) {
        this.name = name;
        this.packageName = packageName;
        this.icon = icon;
    }
}
