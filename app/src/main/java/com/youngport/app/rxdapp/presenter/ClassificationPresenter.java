package com.youngport.app.rxdapp.presenter;

import android.content.Context;

import com.youngport.app.rxdapp.mode.ClassificationMode;
import com.youngport.app.rxdapp.presenter.contract.ClassificationContract;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.RXPresenter;
import com.youngport.app.rxdapp.rxpackage.api.PosService;

import javax.inject.Inject;

/**
 * Created by popper on 2017/12/26.
 */

public class ClassificationPresenter extends RXPresenter<ClassificationContract.View,ClassificationMode> implements ClassificationContract.Presenter {
    @Inject
    public ClassificationPresenter(ApiClass posService, Context context) {
        super(posService, context);
    }

    @Override
    public ClassificationMode getModeInstance() {
        return new ClassificationMode();
    }
}
