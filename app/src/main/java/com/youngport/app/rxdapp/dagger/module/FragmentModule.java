package com.youngport.app.rxdapp.dagger.module;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.youngport.app.rxdapp.rxpackage.ApiClass;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by popper on 2017/11/21.
 */
/*@Singleton*/
@Module
public class FragmentModule {
//    Fragment mFragment;
    Context mContext;

    public FragmentModule(Fragment fragment) {
//        mFragment = fragment;
        mContext = fragment.getContext();
    }

    @Provides
    public Context provideContext() {
        return mContext;
    }

}
