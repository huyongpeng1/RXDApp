package com.youngport.app.rxdapp.rxpackage.api;

import com.youngport.app.rxdapp.base.BaseBean;
import com.youngport.app.rxdapp.model.bean.AccountLoginBean;
import com.youngport.app.rxdapp.model.bean.CreateCouponBean;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import rx.Observable;


/**
 * Created by admin on 2017/6/20.
 */

public interface PosService {

    //账号登陆
    @FormUrlEncoded
    @POST("index.php?g=Post&m=index&a=login")
    Observable<BaseBean<AccountLoginBean>> accountLogin(@Field("timestamp") String timestamp, @Field("phone") String phone, @Field("pwd") String pwd, @Field("mac") String mac);

    //新建优惠券,part传递参数，multipart 传递文件
    @Multipart
    @POST("index.php?s=Api/Coupon/create_coupon")
    Observable<CreateCouponBean> createCoupon(@PartMap Map<String, RequestBody> partMap, @Part MultipartBody.Part... files);


}
