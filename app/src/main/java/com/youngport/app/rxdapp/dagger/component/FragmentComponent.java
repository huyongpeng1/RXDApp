package com.youngport.app.rxdapp.dagger.component;

import com.youngport.app.rxdapp.dagger.module.AppModule;
import com.youngport.app.rxdapp.dagger.module.FragmentModule;
import com.youngport.app.rxdapp.view.fragment.DemoFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by popper on 2017/11/21.
 */
@Singleton
@Component(/*dependencies = AppModule.class,*/ modules = {FragmentModule.class, AppModule.class})
public interface FragmentComponent {
    void inject(DemoFragment demoFragment);
}
