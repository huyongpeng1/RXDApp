package com.youngport.app.rxdapp.adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.youngport.app.rxdapp.R;
import com.youngport.app.rxdapp.databinding.ItemAppinfoBinding;
import com.youngport.app.rxdapp.model.bean.AppInfo;
import com.youngport.app.rxdapp.utils.AppUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by popper on 2017/11/23.
 */

public class AppInfoAdapter extends RecyclerView.Adapter<AppInfoAdapter.AppInfoViewHold> {
    List<AppInfo> mInfoList;
    Context mContext;

    public AppInfoAdapter(List<AppInfo> infoList, Context context) {
        mInfoList = infoList;
        mContext = context;
    }

    @Override
    public AppInfoViewHold onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_appinfo, parent, false);
        return new AppInfoViewHold(inflate);
    }

    @BindingAdapter({"change"})
    public static void changeUi(ImageView imageView, Drawable url) {
        imageView.setBackground(url);
    }

    @Override
    public void onBindViewHolder(AppInfoViewHold holder, int position) {
        AppInfo appInfo = mInfoList.get(position);
        holder.mBind.setAppInfo(appInfo);
    }

    @Override
    public int getItemCount() {
        return mInfoList.size();
    }

    public class AppInfoViewHold extends RecyclerView.ViewHolder {
        private final ItemAppinfoBinding mBind;

        public AppInfoViewHold(final View itemView) {
            super(itemView);
            mBind = DataBindingUtil.bind(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int adapterPosition = getAdapterPosition();
                    AppUtil.uninstallApp(mContext, mInfoList.get(adapterPosition).packageName);
                    mInfoList.remove(adapterPosition);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
