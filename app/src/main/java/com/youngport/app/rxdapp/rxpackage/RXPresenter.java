package com.youngport.app.rxdapp.rxpackage;

import android.content.Context;

import com.youngport.app.rxdapp.base.BaseMode;
import com.youngport.app.rxdapp.base.BasePresenter;
import com.youngport.app.rxdapp.base.BaseView;


/**
 * Created by popper on 2017/11/20.
 */

public abstract class RXPresenter<T extends BaseView, K extends BaseMode> implements BasePresenter<T,K> {

    public T mView;
    public K mMode;
    public ApiClass mApiClass;
    public Context mContext;

    public RXPresenter(ApiClass apiClass, Context context) {
        mApiClass = apiClass;
        mContext = context;
    }

    @Override
    public void attachView(T view) {
        mView = view;
        mMode = getModeInstance();
    }

    @Override
    public void detachView() {
        mView = null;
        mMode.unSubscribe();
        mMode = null;
        mContext =null;
    }


}
