package com.youngport.app.rxdapp.rxpackage;

import com.youngport.app.rxdapp.base.BaseApplication;
import com.youngport.app.rxdapp.base.BaseBean;
import com.youngport.app.rxdapp.model.bean.AccountLoginBean;
import com.youngport.app.rxdapp.rxpackage.api.PosService;
import com.youngport.app.rxdapp.utils.MacUtil;

import rx.Observable;

/**
 * Created by popper on ${DATA}.
 */
//接口实例化统一放在这里
public class ApiClass {

    private final PosService mPosService;

    public ApiClass() {
        mPosService = RetrofitServiceManager.getInstance().create(PosService.class);
    }

    public Observable<BaseBean<AccountLoginBean>> performLogin(String timestamp, String phoneNum, String edtPsd) {
        return mPosService.accountLogin(timestamp, phoneNum, edtPsd, MacUtil.getMac(BaseApplication.getContext()));
    }
}
