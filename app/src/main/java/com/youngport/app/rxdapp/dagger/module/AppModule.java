package com.youngport.app.rxdapp.dagger.module;

import com.youngport.app.rxdapp.base.BaseApplication;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.api.PosService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by popper on 2017/11/20.
 */
//Module 提供通过@Inject注释要实例化所需的参数，通过@Module注释，通过Provides对外提供

@Module
public class AppModule {

//    private ApiClass mApiClass;

    public AppModule() {
    }

    @Singleton
    @Provides
    public ApiClass provideApiClass() {
        return new ApiClass();
     /*   if (mApiClass == null) {
            synchronized (AppModule.class) {
                if (mApiClass == null) {
                    mApiClass = new ApiClass();
                }
            }
        }
        return mApiClass;*/
    }
}
