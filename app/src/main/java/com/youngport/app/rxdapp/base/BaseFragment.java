package com.youngport.app.rxdapp.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.youngport.app.rxdapp.dagger.component.DaggerFragmentComponent;
import com.youngport.app.rxdapp.dagger.component.FragmentComponent;
import com.youngport.app.rxdapp.dagger.module.AppModule;
import com.youngport.app.rxdapp.dagger.module.FragmentModule;
import com.youngport.app.rxdapp.rxpackage.RetrofitServiceManager;
import com.youngport.app.rxdapp.rxpackage.api.PosService;
import com.youngport.app.rxdapp.utils.ToastUtil;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by popper on 2017/11/21.
 */

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements BaseView {
    @Inject
    T mPresenter;
    private Unbinder mBind;
//    public PosService mPosService;
    private FragmentComponent mFragmentComponent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(getLayoutId(), container, false);
        initInject();
        return inflate;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        mPosService = RetrofitServiceManager.getInstance().create(PosService.class);
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        mBind = ButterKnife.bind(this, view);
        initView();
        initData();
        initListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ToastUtil.destoryToast();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        mBind.unbind();

    }

    public FragmentComponent getFragmentComponent() {
        if (mFragmentComponent != null) {
            mFragmentComponent = DaggerFragmentComponent.builder().appModule(new AppModule()).fragmentModule(new FragmentModule(this)).build();
        }
        return mFragmentComponent;
    }

    protected abstract int getLayoutId();

    protected abstract void initInject();

    protected abstract void initListener();

    protected abstract void initData();

    protected abstract void initView();
}
