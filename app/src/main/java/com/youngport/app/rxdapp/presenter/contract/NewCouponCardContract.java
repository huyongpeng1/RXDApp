package com.youngport.app.rxdapp.presenter.contract;

import com.youngport.app.rxdapp.base.BaseView;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by popper on 2017/12/4.
 */

public interface NewCouponCardContract {
    public interface View extends BaseView {
    }

    public interface Presenter {
        void createNewCouponCard(Map<String, RequestBody> partMap, MultipartBody.Part... part);
    }
}
