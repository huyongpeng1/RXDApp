package com.youngport.app.rxdapp.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import com.youngport.app.rxdapp.base.BaseApplication;

import java.util.Stack;

/**
 * Created by willkernel on 2016/11/30.
 * mail:willkerneljc@gmail.com
 */
public class AtyContainer {
    private AtyContainer() {
    }

    private static AtyContainer instance = new AtyContainer();
    @SuppressWarnings("Convert2Diamond")
    private static Stack<Activity> activityStack = new Stack<Activity>();

    public static AtyContainer getInstance() {
        return instance;
    }

    public void addActivity(Activity aty) {
        activityStack.add(aty);
    }

    public void removeActivity(Activity aty) {
        activityStack.remove(aty);
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /**
     * 退出应用程序
     */
    public void ExitApp() {
        try {
            finishAllActivity();
            ActivityManager activityMgr = (ActivityManager) BaseApplication.getContext()
                    .getSystemService(Context.ACTIVITY_SERVICE);
            activityMgr.killBackgroundProcesses(BaseApplication.getContext().getPackageName());
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}