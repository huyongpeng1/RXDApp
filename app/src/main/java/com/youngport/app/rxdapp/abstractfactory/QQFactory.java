package com.youngport.app.rxdapp.abstractfactory;

/**
 * Created by popper on 2017/12/26.
 */

public class QQFactory implements Provider<QQSender> {
    @Override
    public QQSender produce() {
        return new QQSender();
    }
}
