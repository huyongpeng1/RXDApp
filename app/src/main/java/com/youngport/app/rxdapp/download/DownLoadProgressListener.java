package com.youngport.app.rxdapp.download;

/**
 * Created by popper on 2017/12/23.
 */

public interface DownLoadProgressListener {
    void update(long read, long count, boolean done);
}
