package com.youngport.app.rxdapp.view.fragment;

import com.youngport.app.rxdapp.base.BaseFragment;
import com.youngport.app.rxdapp.presenter.DemoFragmentPresenter;
import com.youngport.app.rxdapp.presenter.contract.DemoFragmentContract;

/**
 * Created by popper on 2017/11/21.
 */

public class DemoFragment extends BaseFragment<DemoFragmentPresenter> implements DemoFragmentContract.View {
    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }
}
