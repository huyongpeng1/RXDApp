package com.youngport.app.rxdapp.dagger.module;

import android.app.Activity;
import android.content.Context;

import com.youngport.app.rxdapp.dagger.scope.ActivityScope;
import com.youngport.app.rxdapp.rxpackage.ApiClass;

import dagger.Module;
import dagger.Provides;

/**
 * Created by popper on 2017/11/20.
 */

@Module
public class ActivityModule {

    public Context mContext;

    public ActivityModule(Activity activity) {
        mContext = activity;
    }

    @Provides
    public Context provideContext() {
        return mContext;
    }
}
