package com.youngport.app.rxdapp.constant;

/**
 * Created by admin on 2017/5/27.
 */

public class Constant {

    public static final int USE_KNOW_REQUESTCODE = 1;
    public static final String USE_KNOW_CONTENT = "use_know_content";
    public static final String COUPON_STATE = "coupon_state";
    public static final String MEMBER_CREATE_OR_DETAIL = "member_create_or_detail";
    public static final String PHONE = "phone";
    public static final String PRIVILEGE_KEY = "privilege_key";
    public static final int PRIVILEGE_REQUEST = 3;
    public static final String PRIVILEGE_CONTENT = "privilege_content";
    public static final String USE_KNOW_CONTENT_MEMBER = "use_know_content_member";
    public static final String MEMBER_ID_KEY = "member_id_key";
    public static final int SCREEN_REQUESTCODE = 100;
    public static final String QRCODE_URL = "qrcode_url";
    public static final String COUPON_CARD_OBJECT = "coupon_card_object";
    public static final String IF_PUT = "if_put";
    public static final String TRADE_ID = "trade_id";
    public static final String MERCHANT_NAME = "merchant_name";
    public static final String REBACK_MONEY = "reback_monry";
    public static final String REMARK = "remark";
    public static final String FIND_ORDER = "find_order";
    public static final int WATER_FIND_ORDER_REQUESTCODE = 4;
    public static final String WATER_SCREEN = "water_screen";
    public static final String WATER_TYPE = "0";
    public static final String LOGO_KEY = "logo_key";
    public static final String MESSAGE_DETAIL = "message_detail";
    public static final String SHOPPING_CART_LIST = "shopping_cart_list";
    public static final String COLLECTION_KEY = "collection_key";
    public static final String MEMBER_CARD_ID = "member_card_id";
    public static final String TOTAL_MONEY_KEY = "total_money";
    public static final String UID_KEY = "uid_key";
    public static final String COUPON_PRICE = "coupon_price";
    public static final String NO_GOODS = "no_goods";
    public static final String ORDERSN = "order_sn";
    public static final String SELECT_PAY_WAY_KEY = "select_pay_way";
    public static final String CARD_DE_PRICE = "card_de_price";
    public static final String JI_FEN_USE = "jifenuse";
    public static final String ORDER_AMOUNT = "order_amount";
    public static final String IF_NO_GOODS = "if_no_goods";
    public static final String MAC_ID = "mac_id";
    public static final String CASHIER_UID = "cashier_uid";
    public static final String ISCLOSE = "is_close";
    public static final String ACCOUNT = "account";
    public static final boolean IF_INTERCEPT_HTTP_LOG = true;
    public static final String SCAN_PAY = "扫码";
    public static final String CASH_PAY = "现金";
    public static final String SUCCESS = "success";
    public static final String TRUE = "true";
    public static final String SCAN = "scan";
    public static final String INPUT = "input";
    public static int CONNECT_TIME_OUT = 5;
    public static int READ_TIME_OUT = 30;
    public static String YOUNGPORT_BASE_URL = "http://sy.youngport.com.cn/";
    //    public static String YOUNGPORT_BASE_URL = "http://a.ypt5566.com/";//测试服
    public static int MEMBER_USE_KNOW_REQUESTCODE = 2;
    public static String CASHIER_NAME_KEY = "cashier_name";
    public static String CASHIER_PHONE_KEY = "cashier_phone";
    public static String TOKEN_KEY = "token_key";
    public static String KEY = "26F33A61FAFF49A46475C6C25BD2D561";
    public static String IF_ACCOUNT_LOGIN = "if_account_login";
    public static String SCREEN_KEY = "screen_key";
    public static String COUPON_CARD = "coupon_card";
    public static final String QR_PAY = "付款码";
    public static final String CREDIT_PAY = "刷卡";
}
