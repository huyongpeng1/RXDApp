package com.youngport.app.rxdapp.presenter.contract;

import com.youngport.app.rxdapp.base.BaseView;

/**
 * Created by popper on 2017/11/21.
 */

public interface DemoFragmentContract {
    public interface View extends BaseView {
    }

    public interface Presenter {
    }
}
