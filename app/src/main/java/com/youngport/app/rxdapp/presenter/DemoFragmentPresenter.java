package com.youngport.app.rxdapp.presenter;

import android.content.Context;

import com.youngport.app.rxdapp.mode.DemoFragmentMode;
import com.youngport.app.rxdapp.presenter.contract.DemoFragmentContract;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.RXPresenter;
import com.youngport.app.rxdapp.rxpackage.api.PosService;

import javax.inject.Inject;

/**
 * Created by popper on 2017/11/21.
 */

public class DemoFragmentPresenter extends RXPresenter<DemoFragmentContract.View,DemoFragmentMode> implements DemoFragmentContract.Presenter {
    @Inject
    public DemoFragmentPresenter(ApiClass posService, Context context) {
        super(posService, context);
    }

    @Override
    public DemoFragmentMode getModeInstance() {
        return new DemoFragmentMode();
    }
}
