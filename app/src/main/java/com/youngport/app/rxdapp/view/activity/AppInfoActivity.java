package com.youngport.app.rxdapp.view.activity;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.youngport.app.rxdapp.R;
import com.youngport.app.rxdapp.adapter.AppInfoAdapter;
import com.youngport.app.rxdapp.base.BaseActivity;
import com.youngport.app.rxdapp.base.BaseNoNetWorkActivity;
import com.youngport.app.rxdapp.model.bean.AppInfo;
import com.youngport.app.rxdapp.utils.AppUtil;

import java.util.List;

import butterknife.BindView;

/**
 * Created by popper on 2017/11/23.
 */

public class AppInfoActivity extends BaseNoNetWorkActivity {
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerview;


    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        List<AppInfo> allAppsInfo = AppUtil.getAllAppsInfo(this);
        AppInfoAdapter appInfoAdapter = new AppInfoAdapter(allAppsInfo, this);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerview.setAdapter(appInfoAdapter);
        mRecyclerview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void initView() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_appinfo;
    }
}
