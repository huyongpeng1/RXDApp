package com.youngport.app.rxdapp.rxpackage;

/**
 * Created by popper on 2017/10/16.
 */

public interface RequestListener<T> {
     void onSuccess(T t);

     void onError(Throwable e);

}
