package com.youngport.app.rxdapp.abstractfactory;

import android.content.Context;

import com.youngport.app.rxdapp.mode.AbstractFactoryMode;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.RXPresenter;
import com.youngport.app.rxdapp.rxpackage.api.PosService;

import javax.inject.Inject;

/**
 * Created by popper on 2017/12/26.
 */

public class AbstractFactoryPresenter extends RXPresenter<AbstractFactoryContract.View,AbstractFactoryMode> {
    @Inject
    public AbstractFactoryPresenter(ApiClass posService, Context context) {
        super(posService, context);
    }

    @Override
    public AbstractFactoryMode getModeInstance() {
        return new AbstractFactoryMode();
    }
}
