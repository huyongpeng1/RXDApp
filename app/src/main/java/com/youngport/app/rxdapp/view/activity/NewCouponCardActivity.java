package com.youngport.app.rxdapp.view.activity;

import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.youngport.app.rxdapp.R;
import com.youngport.app.rxdapp.base.BaseActivity;
import com.youngport.app.rxdapp.constant.Constant;
import com.youngport.app.rxdapp.presenter.NewCouponCardPresenter;
import com.youngport.app.rxdapp.presenter.contract.NewCouponCardContract;
import com.youngport.app.rxdapp.utils.CreateTimestamp;
import com.youngport.app.rxdapp.utils.FileUtil;
import com.youngport.app.rxdapp.utils.MD5Util;
import com.youngport.app.rxdapp.utils.PreferenceUtil;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by popper on 2017/12/4.
 */

public class NewCouponCardActivity extends BaseActivity<NewCouponCardPresenter> implements NewCouponCardContract.View {
    @BindView(R.id.iv_back)
    ImageView mIvBack;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_mamual)
    TextView mTvMamual;
    @BindView(R.id.textView2)
    TextView mTextView2;
    @BindView(R.id.edit_merchant_name)
    EditText mEditMerchantName;
    @BindView(R.id.iv_color_piece)
    ImageView mIvColorPiece;
    @BindView(R.id.rl_counpon_color)
    RelativeLayout mRlCounponColor;
    @BindView(R.id.textView4)
    TextView mTextView4;
    @BindView(R.id.edt_coupon_name)
    EditText mEdtCouponName;
    @BindView(R.id.edt_coupon_value)
    EditText mEdtCouponValue;
    @BindView(R.id.edt_mini_expanse)
    EditText mEdtMiniExpanse;
    @BindView(R.id.ll_min_expanse)
    LinearLayout mLlMinExpanse;
    @BindView(R.id.edt_date_left)
    TextView mEdtDateLeft;
    @BindView(R.id.edt_date_right)
    TextView mEdtDateRight;
    @BindView(R.id.edt_put_num)
    EditText mEdtPutNum;
    @BindView(R.id.ll_linearLayout)
    LinearLayout mLlLinearLayout;
    @BindView(R.id.ll_customer_phone)
    LinearLayout mLlCustomerPhone;
    @BindView(R.id.edt_phone_num)
    EditText mEdtPhoneNum;
    @BindView(R.id.tv_use_know)
    TextView mTvUseKnow;
    @BindView(R.id.tv_commit)
    TextView mTvCommit;

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_newcoupon_card_activity;
    }

    @OnClick({R.id.iv_back, R.id.tv_commit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                break;
            case R.id.tv_commit:
                File uploadFile = new File(FileUtil.cacheDir, "icon_example.png");
                FileUtil.saveBitmapToFile(this, uploadFile,
                        BitmapFactory.decodeResource(getResources(), R.drawable.icon_example)
                        , false);
                //这个getUpLoadFiel 可以在p层写个方法，这个是之前没有加mode时候使用的
                MultipartBody.Part part = mPresenter.mMode.getUploadFile(this, uploadFile, "创建优惠券中");
                HashMap<String, String> hashMap = new HashMap<>();
                String timestamp = CreateTimestamp.getTimestamp();
                hashMap.put("timestamp", timestamp);
                String token = PreferenceUtil.getString(this, Constant.TOKEN_KEY);
                hashMap.put("token", token);
                String brand_name = getEditTextContent(mEditMerchantName);
                hashMap.put("brand_name", brand_name);
                String title = getEditTextContent(mEdtCouponName);
                hashMap.put("title", title);
                hashMap.put("color", "Color030");
                String total_price = getEditTextContent(mEdtMiniExpanse);
                hashMap.put("total_price", total_price);
                String de_price = getEditTextContent(mEdtCouponValue);
                hashMap.put("de_price", de_price);
                String quantity = getEditTextContent(mEdtPutNum);
                hashMap.put("quantity", quantity);
                String service_phone = getEditTextContent(mEdtPhoneNum);
                hashMap.put("service_phone", service_phone);
                String begin = CreateTimestamp.getTimestamp();
                hashMap.put("begin_timestamp", begin);
                String endTimestamp = CreateTimestamp.getTimestamp().substring(0, 8) + "88";
                hashMap.put("end_timestamp", endTimestamp);
                hashMap.put("description", "");
                hashMap.put("sign", MD5Util.md5("timestamp=" + timestamp, "token=" + token, "brand_name=" + brand_name, "title=" + title, "color=" + "Color030",
                        "total_price=" + total_price, "de_price=" + de_price, "quantity=" + quantity, "service_phone=" + service_phone, "begin_timestamp=" + begin,
                        "description=" + "", "end_timestamp=" + endTimestamp));
                HashMap<String, RequestBody> uploadParameter = mPresenter.mMode.getUploadParameter(hashMap);
                mPresenter.createNewCouponCard(uploadParameter, part);
                break;
        }
    }
}
