package com.youngport.app.rxdapp.view.activity;

import android.view.View;
import android.widget.Button;

import com.youngport.app.rxdapp.R;
import com.youngport.app.rxdapp.abstractfactory.AbstractFactoryContract;
import com.youngport.app.rxdapp.abstractfactory.AbstractFactoryPresenter;
import com.youngport.app.rxdapp.abstractfactory.QQFactory;
import com.youngport.app.rxdapp.abstractfactory.QQSender;
import com.youngport.app.rxdapp.abstractfactory.WXFactory;
import com.youngport.app.rxdapp.abstractfactory.WXSender;
import com.youngport.app.rxdapp.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by popper on 2017/12/26.
 */

public class AbstractFactoryActivity extends BaseActivity<AbstractFactoryPresenter> implements AbstractFactoryContract.View{
    @BindView(R.id.btn_qq)
    Button mBtnQq;
    @BindView(R.id.btn_wx)
    Button mBtnWx;

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_abstract_factory;
    }

    @OnClick({R.id.btn_qq, R.id.btn_wx})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_qq:
                QQFactory qqFactory = new QQFactory();
                QQSender qqSender = qqFactory.produce();
                qqSender.send();
                break;
            case R.id.btn_wx:
                WXFactory wxFactory = new WXFactory();
                WXSender wxSender = wxFactory.produce();
                wxSender.send();
                break;
        }
    }
}
