package com.youngport.app.rxdapp.presenter.contract;

import com.youngport.app.rxdapp.base.BaseView;
import com.youngport.app.rxdapp.model.bean.AccountLoginBean;

/**
 * Created by popper on 2017/11/20.
 */

public interface MainActivityContract {
    public interface View extends BaseView {
        void accountLoginSuccess(AccountLoginBean accountLoginBean);
    }

    public interface Presenter {
        void performAccountLogin(String timestamp, String phoneNum, String edtPsd);
    }
}
