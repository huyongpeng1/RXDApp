package com.youngport.app.rxdapp.base;

import android.app.Application;

import com.youngport.app.rxdapp.rxpackage.RetrofitServiceManager;
import com.youngport.app.rxdapp.rxpackage.api.PosService;

/**
 * Created by popper on 2017/11/20.
 */

public class BaseApplication extends Application {
    public static BaseApplication instance;
//    public static PosService mPosService;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        mPosService = RetrofitServiceManager.getInstance().create(PosService.class);
    }

    public static BaseApplication getContext() {

        return instance;
    }


}
