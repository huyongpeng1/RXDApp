package com.youngport.app.rxdapp.abstractfactory;

import com.youngport.app.rxdapp.base.BaseView;

/**
 * Created by popper on 2017/12/26.
 */

public interface AbstractFactoryContract {
    public interface View extends BaseView {
    }

    public interface Presenter {
    }
}
