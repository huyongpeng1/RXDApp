package com.youngport.app.rxdapp.view.activity;

import com.youngport.app.rxdapp.base.BaseActivity;
import com.youngport.app.rxdapp.presenter.ScaleSelectorPresenter;
import com.youngport.app.rxdapp.presenter.contract.ScaleSelectorContract;

/**
 * Created by popper on 2017/12/26.
 */

public class ScaleSelectorActivity extends BaseActivity<ScaleSelectorPresenter> implements ScaleSelectorContract.View {
    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
    }

    @Override
    public int getLayoutId() {
        return 0;
    }
}
