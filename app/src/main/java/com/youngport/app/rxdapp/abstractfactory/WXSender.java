package com.youngport.app.rxdapp.abstractfactory;

import com.youngport.app.rxdapp.utils.ToastUtil;

/**
 * Created by popper on 2017/12/26.
 */

public class WXSender implements Sender {
    @Override
    public void send() {
        ToastUtil.showToastCenter("微信发消息！");
    }
}
