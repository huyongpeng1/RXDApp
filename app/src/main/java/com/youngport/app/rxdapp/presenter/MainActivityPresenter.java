package com.youngport.app.rxdapp.presenter;

import android.content.Context;
import android.util.Log;

import com.youngport.app.rxdapp.base.BaseBean;
import com.youngport.app.rxdapp.constant.Constant;
import com.youngport.app.rxdapp.mode.MainActivityMode;
import com.youngport.app.rxdapp.model.bean.AccountLoginBean;
import com.youngport.app.rxdapp.presenter.contract.MainActivityContract;
import com.youngport.app.rxdapp.rxpackage.ApiClass;
import com.youngport.app.rxdapp.rxpackage.RXPresenter;
import com.youngport.app.rxdapp.rxpackage.RequestListener;
import com.youngport.app.rxdapp.rxpackage.api.PosService;
import com.youngport.app.rxdapp.utils.MacUtil;
import com.youngport.app.rxdapp.utils.ToastUtil;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by popper on 2017/11/20.
 */

public class MainActivityPresenter extends RXPresenter<MainActivityContract.View, MainActivityMode> implements MainActivityContract.Presenter {
    @Inject
    public MainActivityPresenter(ApiClass apiClass, Context context) {
        super(apiClass, context);
    }

    @Override
    public void performAccountLogin(String timestamp, String phoneNum, String edtPsd) {
        Observable<BaseBean<AccountLoginBean>> accountLoginBeanObservable = mApiClass.performLogin(timestamp, phoneNum, edtPsd);
        mMode.doHttpDeal(mContext, accountLoginBeanObservable, new RequestListener<AccountLoginBean>() {

            @Override
            public void onSuccess(AccountLoginBean accountLoginBean) {
                mView.accountLoginSuccess(accountLoginBean);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("way", "onError: " + e.getMessage());
            }
        });
    }

    @Override
    public MainActivityMode getModeInstance() {
        return new MainActivityMode();
    }
}
