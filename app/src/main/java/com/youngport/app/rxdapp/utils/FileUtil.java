package com.youngport.app.rxdapp.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.youngport.app.rxdapp.base.BaseApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2017/7/19.
 */

public class FileUtil {

    public static File cacheDir = !isExternalStorageWritable() ? BaseApplication.getContext().getFilesDir()
            : BaseApplication.getContext().getExternalCacheDir();

    /**
     * 判断外部存储是否可用
     */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * 保存图片到本地
     */
    public static Uri saveBitmapToFile(Context context, File imageFile, Bitmap bitmap, boolean isShare) {
        Uri uri = Uri.fromFile(imageFile);
        if (isShare && imageFile.exists()) {
            return uri;
        }
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            boolean isCompress = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            if (isCompress) {
                Log.i("message", "success");
            } else {
                Log.i("message", "failure");
            }
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("message", "failure");
        }
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(), imageFile.getAbsolutePath(), imageFile.getName(), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
        return uri;
    }


}
