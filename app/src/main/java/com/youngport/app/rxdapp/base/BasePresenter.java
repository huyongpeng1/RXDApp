package com.youngport.app.rxdapp.base;

/**
 * Created by popper on 2017/11/20.
 */

public interface BasePresenter<T extends BaseView, K extends BaseMode> {
    void attachView(T view);

    void detachView();

    K getModeInstance();

}
